
package estudiante;

import javax.inject.Named;



@Named(value = "prueba")

public class Candidato {

    
    public Candidato() {
    }
    
    private String nombre ;

    
    public String getNombre() {
        return nombre;
    }

    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    private String contraseña;

   
    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
    

}
